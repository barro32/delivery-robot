$(document).ready(function() {

	// initialise Materialize select input
	$('#tests').material_select();

	// set up canvas
	let canvas = document.getElementById('canvas');
	let ctx = canvas.getContext('2d');
	ctx.canvas.height = ctx.canvas.width;
	ctx.transform(1, 0, 0, -1, 0, ctx.canvas.height);
	ctx.translate(ctx.canvas.width * 0.5, ctx.canvas.height * 0.5);

	// event bindings
	$('#addCrate').on('click', addCrate);
	$('.crates').on('click', '.remove-crate', removeCrate);
	$('#tests').on('change', runTest);

	function addCrate() {
		let crateN = $('.crate').length;
		$('.crates').append(
			'<div class="crate row valign-wrapper">'+
				'<div class="col s3">'+
					'<button id="remove-crate-'+crateN+'" class="remove-crate btn waves-effect waves-light red input"><i class="material-icons left">remove_circle_outline</i>Crate</button>'+
				'</div>'+
				'<div class="input-field col s3">'+
					'<label for="crate-x'+crateN+'">X</label>'+
					'<input id="crate-x'+crateN+'" type="number" class="validate input crate-x" required="required">'+
				'</div>'+
				'<div class="input-field col s3">'+
					'<label for="crate-y'+crateN+'">Y</label>'+
					'<input id="crate-y'+crateN+'" type="number" class="validate input crate-y" required="required">'+
				'</div>'+
				'<div class="input-field col s3">'+
					'<label for="crate-boxes'+crateN+'">Boxes</label>'+
					'<input id="crate-boxes'+crateN+'" type="number" class="validate input crate-boxes" required="required">'+
				'</div>'+
			'</div>'
		);
	}

	function removeCrate() {
		$(this).parents('.crate').remove();
	}

	function runTest() {
		if($(this).val() === "custom") {
			formDisabled(false);
		} else {
			formDisabled(true);
			let inputs = eval($(this).val());
			Robot(inputs[0], inputs[1], inputs[2], inputs[3], ctx, $('#animate').prop('checked'));
		}
	}

	function formDisabled(bool) {
		$('.input').prop('disabled', bool);
	}

});

function submitForm() {
	let conX = Number($('#conveyor-x').val());
	let conY = Number($('#conveyor-y').val());
	let posX = Number($('#robot-x').val());
	let posY = Number($('#robot-y').val());
	let crates = $('.crate');
	let crateArray = [];
	for(let i = 0; i < crates.length; i++) {
		
		crateArray.push([
			Number($(crates[i]).find('.crate-x').val()), 
			Number($(crates[i]).find('.crate-y').val()), 
			Number($(crates[i]).find('.crate-boxes').val())
		]);
	}
	let instructions = $('#instructions').val().toUpperCase();
	let animate = $('#animate').prop('checked');


	let $canvas = $('#canvas');
	let ctx = $canvas[0].getContext('2d');

	Robot([conX, conY], [posX, posY], crateArray, instructions, ctx, animate);
}