Robot = function(conveyor, pos, crates, instructions, ctx, animate) {

	this.status = "OK";
	this.conveyor = {x: conveyor[0], y: conveyor[1]};
	this.pos = {x: pos[0], y: pos[1]};
	this.crates = crates.map(function(c) {
		return {x: c[0], y: c[1], bags: c[2]};
	});
	this.instructions = instructions.split("");
	this.bags = 0;
	this.droppedOffBags = 0;

	animate ? this.time = 800 : this.time = 0;

	this.action = function() {
		if(this.instructions.length && this.status === "OK") {
			if(animate) this.render();
			
			switch (this.instructions.shift()) {
				case "N":
					this.pos.y++;
					break;
				case "S":
					this.pos.y--;
					break;
				case "E":
					this.pos.x++;
					break;
				case "W":
					this.pos.x--;
					break;
				case "P":
					this.pickUpBag();
					break;
				case "D":
					this.dropOffBags();
					break;
				default:
					console.log("instructions: " + this.instructions);
			}
		} else {
			if(!animate) this.render();
			clearInterval(interval);
			this.displayOutput();
		}
	}

	this.pickUpBag = function() {
		let overCrate = false;
		this.crates.forEach(function(crate) {
			if(this.pos.x === crate.x && this.pos.y === crate.y) {
				overCrate = true;
				if(crate.bags > 0) {
					this.bags++;
					crate.bags--;
				}
			}
		});
		if(!overCrate) {
			this.status = "BROKEN";
		}
	}

	this.dropOffBags = function() {
		if(this.pos.x === this.conveyor.x && this.pos.y === this.conveyor.y) {
			this.droppedOffBags = this.bags;
			this.bags = 0;
		} else {
			this.status = "BROKEN";
		}
	}

	this.render = function() {
		// background
		ctx.fillStyle = 'rgb(0,0,0)';
		ctx.fillRect(ctx.canvas.width*-0.5, ctx.canvas.height*-0.5, ctx.canvas.width, ctx.canvas.height);
		// grid
		ctx.beginPath();
		ctx.strokeStyle='rgb(50, 50, 50)';
		for(let i = -ctx.canvas.width; i < ctx.canvas.width; i+=10) {
			ctx.moveTo(i, ctx.canvas.height);
			ctx.lineTo(i, -ctx.canvas.height);
			ctx.moveTo(-ctx.canvas.width, i);
			ctx.lineTo(ctx.canvas.width, i);
		}
		ctx.stroke();
		// axes
		ctx.beginPath();
		ctx.strokeStyle='rgb(100, 100, 100)';
		ctx.moveTo(0, ctx.canvas.height);
		ctx.lineTo(0, -ctx.canvas.height);
		ctx.moveTo(-ctx.canvas.height, 0);
		ctx.lineTo(ctx.canvas.height, 0);
		ctx.stroke();
		// conveyor
		ctx.fillStyle = 'rgb(0, 200, 0)';
		ctx.fillRect(this.conveyor.x*10, this.conveyor.y*10, 10, 10)
		// crates
		ctx.fillStyle = 'rgb(0, 0, 255)';
		this.crates.forEach(function(crate) {
			ctx.fillRect(crate.x*10, crate.y*10, 10, 10);
		});
		// robot
		ctx.beginPath();
		ctx.arc(this.pos.x*10+5, this.pos.y*10+5, 5, 0, 2*Math.PI);
		ctx.fillStyle = 'red';
		ctx.fill();
	}

	this.displayOutput = function() {
		$('.running').text('finished')
		$('#bags').text(this.droppedOffBags);
		$('#status').text(this.pos.x + " " + this.pos.y + " " + this.status);
	}
	this.clearOutput = function() {
		$('.running').text('...running');
		$('#bags').text('');
		$('#status').text('');
	}

	// run
	this.clearOutput();
	let interval = setInterval(this.action, this.time);

}