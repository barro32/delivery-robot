# Delivery Robot

Simulate a robot that can move, pick up bags from crates and drop them off on a conveyor belt.

## Getting Started
  
* Open `index.html`
* Run a test command or enter your own inputs
* Use `Animate` to step through and display each instruction
* View `Output`

## Built With

* [Materialize](http://materializecss.com/) - Styling

## Author

* **Daniel Barrington**